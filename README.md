# MagmiWeb2 #

This project is meant to merely be a reskin of the Magmi Web interface. The layout is also a little more flexible in terms of adding new features and content.

Currently the interface is incomplete and thus is not really usable yet, but it is getting there.

### Setup ###

* Install Magmi
* place the 'magmiweb2' folder into the root of the Magmi installation.
* Profit