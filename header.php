<?php require_once("head.php")?>
<div class="row header bump-top">
	<div class="col-lg-4">
		<div class="cell-body">
			<div class="logo"><img src="img/logo.png"/></div>
			<span class="dewi indent">Logo Design by dewi</span>
		</div>
	</div>
	<div class="col-lg-5">
		<div class="cell-inner-container brdr-1-sld-purple">
			<div class="header-cell-title h3 bg-gradient-purple title-fnt-white">
				Release Information
			</div>
			<div class="cell-body">
				<ul class="rel-info list-group">
				  <li class="list-group-item">Version: <?php echo Magmi_Version::$version?></li>
				  <li class="list-group-item">Author: <a href="mailto:dweeves@gmail.com">Dweeves</a></li>
				  <li class="list-group-item">License: MIT OSL License</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="cell-inner-container brdr-1-sld-purple">
			<div class="header-cell-title h3 bg-gradient-purple title-fnt-white">
				Support Magmi!
			</div>
			<div class="cell-body">
				<div class="donate alert alert-info text-center">
					If you love Magmi, help out!<hr>
					<a class="btn btn-success" href="http://sourceforge.net/donate/index.php?group_id=350817" target="_blank">Donate</a>
				</div>				
			</div>
		</div>
	</div>
</div>