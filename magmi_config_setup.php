<?php
require_once ("magmi_config.php");
require_once ("magmi_statemanager.php");
require_once ("dbhelper.class.php");
$conf = Magmi_Config::getInstance();
$conf -> load();
$conf_ok = 1;
?>
<?php
$profile = "";
if (isset($_REQUEST["profile"])) {
    $profile = $_REQUEST["profile"];
} else {

    if (isset($_SESSION["last_runned_profile"])) {
        $profile = $_SESSION["last_runned_profile"];
    }
}
if ($profile == "") {
    $profile = "default";
}
$eplconf = new EnabledPlugins_Config($profile);
$eplconf -> load();
if (!$eplconf -> hasSection("PLUGINS_DATASOURCES")) {
    $conf_ok = 0;
}
?>
<!-- MAGMI UPLOADER -->

<?php $zipok = class_exists("ZipArchive"); ?>
<div class="row bump-top">
	<div class="col-lg-12">
		<div class="cell-innercontainer bg-gradient-purple cell-title title-fnt-white">
			<span>Update Magmi</span>
		</div>
	</div>
</div>
<div class="section-wrapper">
	<?php if($zipok){?>
	<div class="row">
		<form method="post" enctype="multipart/form-data" action="magmi_upload.php">
				<div class="col-lg-6">
					<div class="cell-innercontainer sub-section-wrapper">
						<div class="bg-purple cell-subtitle">
							<span>Update Magmi Release</span>
						</div>
						<div class="section-innercontainer">
						<input type="file" name="magmi_package" class="btn inline-width-50 std-input"></input><input type="submit" value="Upload Magmi Release" class="btn btn-default inline-width-50 std-input"></input>
						<?php if(isset($_SESSION["magmi_install"])){
							$type=$_SESSION["magmi_install"][0];
							$msg=$_SESSION["magmi_install"][1];
						?>
						<div class="mgupload_<?php echo $type?>">
							<?php echo $msg; ?>
						</div>
						<?php
                        unset($_SESSION["magmi_install"]);
                        }
                    ?>
						</div>
					</div>
				</div>
		</form>
	<!--  PLUGIN UPLOADER -->
		<form method="post" enctype="multipart/form-data" action="plugin_upload.php">
				<div class="col-lg-6">
					<div class="cell-innercontainer sub-section-wrapper">
						<div class="bg-purple cell-subtitle">
							<span>Upload New Plugins</span>
						</div>
						<div class="section-innercontainer">
							<input type="file" name="plugin_package" class="btn inline-width-50 std-input"></input><input type="submit" value="Upload Plugins" class="btn btn-default inline-width-50 std-input"></input>
							<?php if(isset($_SESSION["plugin_install"])){
									$type=$_SESSION["plugin_install"][0];
									$msg=$_SESSION["plugin_install"][1];
								?>
							<div class="plupload_$type">
							<?php echo $msg; ?>
							</div>
							<?php unset($_SESSION["magmi_install"]);
                                }
							?>
						</div>
					</div>
				</div>
		</form>
	</div>
	<?php } else { ?>
	<div class="row">
		<div class="grid_12 col"><h3>Update Disabled</h3>	
			<div class="error">
				Zip library not available, Upgrade/Upload function are not enabled
			</div>
		</div>
	</div>
	<?php } ?>
</div>

<div class="row bump-top">
	<div class="col-lg-12">
		<div class="cell-innercontainer bg-gradient-purple cell-title title-fnt-white">
			<span>Run Magmi</span>
			<?php if(!$conf_ok){?>
			<span class="saveinfo log_warning"><b>No Profile saved yet, Run disabled!!</b></span>
			<?php } ?>
		</div>
	</div>
</div>
<div class="section-wrapper">
	<div class="row">
		<form method="POST" id="runmagmi" action="magmi.php" <?php if(!$conf_ok){?>style="display:none"<?php } ?>>
			<input type="hidden" name="run" value="import"></input>
			<input type="hidden" name="logfile" value="<?php echo Magmi_StateManager::getProgressFile()?>"></input>
			<div class="col-lg-12">
				<div class="cell-innercontainer sub-section-wrapper">
					<div class="bg-purple cell-subtitle">
						<span>Directly Run Magmi With An Existing Profile</span>
					</div>
					<div class="section-innercontainer">
							<span>Run Magmi With Profile:</span>
							<?php $profilelist = $conf -> getProfileList(); ?>
							<select name="profile" id="runprofile" class="std-input">
								<option <?php if(null==$profile){?>selected="selected"<?php } ?> value="default">Default</option>
								<?php foreach($profilelist as $profilename){?>
								<option <?php if($profilename==$profile){?>selected="selected"<?php } ?> value="<?php echo $profilename?>"><?php echo $profilename?></option>
								<?php } ?>
							</select>
						<span>using mode:</span>
							<select name="mode" id="mode" class="std-input">
								<option value="update">Update existing items only,skip new ones</option>
								<option value="create">create new items &amp; update existing ones</option>
								<option value="xcreate">create new items only, skip existing ones</option>
							</select>
						<input class="std-input btn btn-default pull-right" type="submit" value="Run Import" <?php if(!$conf_ok){?>disabled="disabled"<?php } ?>></input>
					</div>
				</div>				
			</div>
		</form>
	</div>
</div>
<div class="row bump-top">
	<div class="col-lg-12">
		<div class="cell-innercontainer">
			<a href="magmi_utilities.php" class="label label-primary">Advanced Utilities</a>
		</div>
	</div>
</div>
<div class="row bump-top">
	<div class="col-lg-12">
		<div class="cell-innercontainer bg-gradient-purple cell-title title-fnt-white">
			<span>Configure Global Parameters</span>
			<span id="commonconf_msg" class="pull-right">Saved:<?php echo $conf->getLastSaved("%c")?></span>
		</div>
	</div>
</div>

<?php
$cansock = true;
$dmysqlsock = DBHelper::getMysqlSocket();
$cansock = !($dmysqlsock === false);
?>	
<div class="section-wrapper">
	<div class="row">
		<form method="post" action="magmi_saveconfig.php" id="commonconf_form">
			<div class="col-lg-4">
				<div class="cell-innercontainer sub-section-wrapper">
					<div class="bg-purple cell-subtitle">
						<span>Database</span>
					</div>
					<div class="section-innercontainer">
						<?php $curconn = $conf -> get("DATABASE", "connectivity", "net"); ?>
						<div class="form-group">
							<label for="DATABASE:connectivity" class="control-label h6">Connectivity</label>
							<select name="DATABASE:connectivity" id="DATABASE:connectivity" class="form-control">
								<option value="net" <?php if($curconn=="net"){?>selected="selected"<?php } ?>>Using host/port</option>
								<?php if($cansock){?>
								<option value="socket" <?php if($curconn=="socket"){?>selected="selected"<?php } ?>>Using local socket</option>
								<?php } ?>
							</select>
						</div>						
						<div id="connectivity:net" class="connectivity" <?php if($curconn!="net"){?>style="display:none"<?php } ?>>
							<div class="form-group">
								<label for="DATABASE:host" class="control-label h6">Host:</label>
								<input id="DATABASE:host" type="text" name="DATABASE:host" value="<?php echo $conf->get("DATABASE","host","localhost")?>" class="form-control"></input>
							</div>
							<div class="form-group">
								<label for="DATABASE:port" class="control-label h6">Port:</label>
								<input id="DATABASE:port" type="text" name="DATABASE:port" value="<?php echo $conf->get("DATABASE","port","3306")?>" class="form-control"></input>
							</div>
						</div>
						<?php if($cansock){?>
						<div id="connectivity:socket" class="connectivity" <?php if($curconn!="socket"){?>style="display:none"<?php  } ?>>
							<div class="form-group">
								<label for="DATABASE:unix_socket" class="control-label h6">Unix Socket:</label>							
								<?php
                                $mysqlsock = $conf -> get("DATABASE", "unix_socket", $dmysqlsock);
                                if (!file_exists($mysqlsock)) {
                                    $mysqlsock = $dmysqlsock;
                                }
								?>
								<input type="text" id="DATABASE:unix_socket" name="DATABASE:unix_socket" value="<?php echo $mysqlsock?>" class="form-control"></input>
							</div>
						</div>
						<?php } ?>
						<hr />
					    <div class="form-group">
                            <label for="DATABASE:dbname" class="control-label h6">Database Name</label>
                            <input id="DATABASE:dbname" type="text" name="DATABASE:dbname" value="<?php echo $conf->get("DATABASE","dbname")?>" class="form-control"></input>
                        </div>
                        <div class="form-group">
                            <label for="DATABASE:user" class="control-label h6">User Name</label>
                            <input id="DATABASE:user" type="text" name="DATABASE:user" value="<?php echo $conf->get("DATABASE","user")?>" class="form-control"></input>
                        </div>
                        <div class="form-group">
                            <label for="DATABASE:password" class="control-label h6">Password</label>
                            <input id="DATABASE:password" type="password" name="DATABASE:password" value="<?php echo $conf->get("DATABASE","password")?>" class="form-control"></input>
                        </div>
                        <div class="form-group">
                            <label for="DATABASE:table_prefix" class="control-label h6">Table Prefix</label>
                            <input id="DATABASE:table_prefix" type="text" name="DATABASE:table_prefix" value="<?php echo $conf->get("DATABASE","table_prefix")?>" class="form-control"></input>
                        </div>				
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="cell-innercontainer sub-section-wrapper">
					<div class="bg-purple cell-subtitle">
						<span>Magento</span>
					</div>
					<div class="section-innercontainer">
					    <div class="form-group">
                            <label for="MAGENTO:version" class="control-label h6">Version</label>
                            <select name="MAGENTO:version" id="MAGENTO:version" class="form-control">           
                                <?php foreach(array("1.9.x","1.8.x","1.7.x","1.6.x","1.5.x","1.4.x","1.3.x") as $ver){?>
                                    <option value="<?php echo $ver?>"
                                        <?php if($conf->get("MAGENTO","version")==$ver){?>
                                            selected=selected <?php }?>><?php echo $ver?></option>
                                        <?php }?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="MAGENTO:basedir" class="control-label h6">Filesystem Path to Magento Root Directory</label>
                            <input id="MAGENTO:basedir" type="text" name="MAGENTO:basedir" value="<?php echo $conf->get("MAGENTO","basedir")?>" class="form-control"></input>
                        </div>  
					</div>					
				</div>			
			</div>
			<div class="col-lg-4">
				<div class="cell-innercontainer sub-section-wrapper">
					<div class="bg-purple cell-subtitle">
						<span>Global</span>
					</div>
					<div class="section-innercontainer">
                        <div class="form-group"  id="globstep">
                            <label for="GLOBAL:step" class="control-label h6">Reporting step in %</label>
                            <input id="GLOBAL:step" type="text" name="GLOBAL:step" value="<?php echo $conf->get("GLOBAL","step")?>" size="5" class="form-control"></input>
                        </div>
					</div>
					<div class="bg-purple cell-subtitle">
						<span>Dir &amp; File permissions</span>
					</div>
					<div class="section-innercontainer">
					</div>					
				</div>			
			</div>			
			<!--
			<div class="container_12" id="common_config">
				<div class="grid_4 col">
					<h3>Database</h3>
					
					<?php $curconn = $conf -> get("DATABASE", "connectivity", "net"); ?>
					<ul class="formline">
						<li class="label">Connectivity</li>
						<li class="value"><select name="DATABASE:connectivity" id="DATABASE:connectivity">
							<option value="net" <?php if($curconn=="net"){?>selected="selected"<?php } ?>>Using host/port</option>
							<?php if($cansock){?>
							<option value="socket" <?php if($curconn=="socket"){?>selected="selected"<?php } ?>>Using local socket</option>
							<?php } ?>
						</select></li>
					</ul>
						
					<div id="connectivity:net" class="connectivity" <?php if($curconn!="net"){?>style="display:none"<?php } ?>>
					<ul class="formline">				
						<li class="label">Host:</li>
						<li class="value"><input type="text" name="DATABASE:host" value="<?php echo $conf->get("DATABASE","host","localhost")?>" ></input></li>
					</ul><ul class="formline">
						<li class="label">Port:</li>
						<li class="value"><input type="text" name="DATABASE:port" value="<?php echo $conf->get("DATABASE","port","3306")?>" ></input></li>
					</ul>
					</div>
					<?php if($cansock){?>
					<div id="connectivity:socket" class="connectivity" <?php if($curconn!="socket"){?>style="display:none"<?php  } ?>>
					<ul class="formline">
						<li class="label">Unix Socket:</li>
						
						<?php
						$mysqlsock = $conf -> get("DATABASE", "unix_socket", $dmysqlsock);
						if (!file_exists($mysqlsock)) {
							$mysqlsock = $dmysqlsock;
						}
						?>
						<li class="value"><input type="text" name="DATABASE:unix_socket" value="<?php echo $mysqlsock?>" ></input></li>
					</ul>
					</div>
					<?php } ?>	
					<hr/>
				
					<ul class="formline">
						<li class="label">DB Name:</li>
						<li class="value"><input type="text" name="DATABASE:dbname" value="<?php echo $conf->get("DATABASE","dbname")?>" ></input></li>
					</ul>
					
					<ul class="formline">
						<li class="label">Username:</li>
						<li class="value"><input type="text" name="DATABASE:user" value="<?php echo $conf->get("DATABASE","user")?>" ></input></li>
					</ul>
					<ul class="formline">
						<li class="label">Password:</li>
						<li class="value"><input type="password" name="DATABASE:password" value="<?php echo $conf->get("DATABASE","password")?>" ></input></li>
					</ul>
					<ul class="formline">
						<li class="label">Table prefix:</li>
						<li class="value"><input type="text" name="DATABASE:table_prefix" value="<?php echo $conf->get("DATABASE","table_prefix")?>" ></input></li>
					</ul>
					</div>
					<div class="grid_4 col">
					<h3>Magento</h3>
					<ul class="formline">
						<li class="label">Version:</li>
						<li class="value"><select name="MAGENTO:version">
							<?php foreach(array("1.7.x","1.6.x","1.5.x","1.4.x","1.3.x") as $ver){?>
								<option value="<?php echo $ver?>" <?php if($conf->get("MAGENTO","version")==$ver){?>selected=selected<?php } ?>><?php echo $ver?></option>
							<?php } ?>
						</select></li>
					</ul>
					<ul class="formline" style="height:40px">
						<li class="label">Filesystem Path to magento directory:</li>
						<li class="value"><input type="text" name="MAGENTO:basedir" value="<?php echo $conf->get("MAGENTO","basedir")?>" ></input></li>
					</ul>
					
					</div>
					<div class="grid_4 col omega">
					<h3>Global</h3>
					<ul class="formline" id="globstep" >
						<li class="label">Reporting step in %:</li>
						<li class="value"><input type="text" name="GLOBAL:step" size="5" value="<?php echo $conf->get("GLOBAL","step")?>"></input></li>
					</ul>
					<ul class="formline" id="mssep" >
						<li class="label">Multiselect value separator:</li>
						<li class="value"><input type="text" name="GLOBAL:multiselect_sep" size="3" value="<?php echo $conf->get("GLOBAL","multiselect_sep",",")?>"></input></li>
					</ul>
					<h3>Dir &amp; File permissions</h3>
					<ul class="formline" id="dirperms">
						<li class="label">Directory permissions:</li>
						<li class="value"><input type="text" name="GLOBAL:dirmask" size="3" value="<?php echo $conf->get("GLOBAL","dirmask","755")?>"></input></li>
					</ul>
					<ul class="formline" id="fileperms">
						<li class="label">File permissions:</li>
						<li class="value"><input type="text" name="GLOBAL:filemask" size="3" value="<?php echo $conf->get("GLOBAL","filemask","644")?>"></input></li>
					</ul>
				
					</div>
					<div class="clear"></div>
				
					<div class="container_12">
					<div class="grid_12">
						<div style="float:right">
							<a id="save_commonconf" class="actionbutton" href="#">Save global parameters</a>
						</div>
					</div>
				</div>
			</div>
			-->
		</form>
	</div>
</div>

<div class="clear"></div>
<script type="text/javascript">
    $('save_commonconf').observe('click', function() {
        new Ajax.Updater('commonconf_msg', "magmi_saveconfig.php", {
            parameters : $('commonconf_form').serialize('true'),
            onSuccess : function() {
                $('commonconf_msg').show();
            }
        });
    }); 
<?php if($conf_ok){?>
    $('runprofile').observe('change', function(ev) {
        document.location = 'magmi.php?profile=' + Event.element(ev).value;
    }); 
<?php } ?>
    $('DATABASE:connectivity').observe('change', function(ev) {
        var clist = $$('.connectivity');
        clist.each(function(it) {
            var el = it;
            if (el.id == 'connectivity:' + $F('DATABASE:connectivity')) {
                el.show();
            } else {
                el.hide();
            }
        });

    }); 
</script>